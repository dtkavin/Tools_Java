import org.joda.time.DateTime;

/**
 * Created by zhangzhiyong on 2016/7/21.
 */
public class JodaTimeDemo {
    public static void main(String[] args) {
        //时分秒毫秒
        DateTime dateTime = new DateTime(2016, 7, 11, 12, 23, 59, 222);
        //时分秒毫秒+时区
//        dateTime = new DateTime(2016, 7, 11, 12, 23, 59, 222, DateTimeZone.UTC);

        dateTime= dateTime.plusDays(5);

        dateTime=dateTime.minusDays(2);

        dateTime=dateTime.withDayOfMonth(1);

        System.out.println(dateTime);
        System.out.println(dateTime.year().getAsText());
    }
}
